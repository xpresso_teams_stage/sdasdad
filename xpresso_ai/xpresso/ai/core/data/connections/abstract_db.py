""" Abstract class for database supported sources in Data-Connectivity """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'

import abc
from xpresso.ai.core.data.connections.abstract import AbstractDataConnector


class AbstractDBConnector(AbstractDataConnector):
    """

    Abstract Base Class for the DataConnector

    """

    @classmethod
    @abc.abstractmethod
    def getlogger(cls):
        """

        Abstract method to instantiate the Xpresso Logger Module

        """

    @classmethod
    @abc.abstractmethod
    def connect(cls, config):
        """

        Abstract method to establish client-side connection with
        various datasources

        """

    @classmethod
    @abc.abstractmethod
    def import_dataframe(cls, config):
        """

        Abstract method for importing data from various datasources

        """

    @classmethod
    @abc.abstractmethod
    def close(cls):
        """

        Abstract method to close connection to a datasource

        """
